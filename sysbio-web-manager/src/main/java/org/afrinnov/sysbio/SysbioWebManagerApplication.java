package org.afrinnov.sysbio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SysbioWebManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SysbioWebManagerApplication.class, args);
    }

}
